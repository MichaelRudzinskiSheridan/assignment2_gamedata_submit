﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour
{
    //these act as folders. Each object type is stored here as prefabs
    public Transform groundHolder;
    public Transform pickupHolder;
    public Transform turretHolder;

    //Base64 is binary and CSV holds more data but is a heavier file type. For our purposes, we will use Base64
    public enum MapDataFormat
    {
        Base64,
        CSV
    }
    //This stores the type of file we are using. We will use this later
    public MapDataFormat mapDataFormat;
    //The sprite texture holds all the types of sprites in a grid shape
    public Texture2D spriteSheetTexture;

    //These are all the prefabs that will populate the world. They are stored in arraylists
    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject pickupPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> pickupPositions;

    //This is used to organize a grid shape based on the tiles
    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    //The name of the map file
    public string TMXFilename;

    //These are the names needed for Unity to find and load the map file
    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;


    //The dimensions for each tile
    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    //The map data name. The TMX file and if there are multiple ones, they are stored in the array list
    public string mapDataString;
    public List<int> mapData;



    //Clears the console
    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole()
    {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    //This destroys all the prefabs in the map before loading in new ones
    static void DestroyChildren(Transform parent)
    {
        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET
    //Once the editor and map is cleared, we will load in the new map

    public void LoadLevel()
    {

        ClearEditorConsole();
        DestroyChildren(groundHolder);

        DestroyChildren(turretHolder);
        DestroyChildren(pickupHolder);

        // This is how Unity creates a path and finds the way to the "Maps" folder
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // The TMX file is cleared first then all the string data is parsed into cohesive and understandable chunks
        {
            mapData.Clear();

            string content = File.ReadAllText(TMXFile);

            using (XmlReader reader = XmlReader.Create(new StringReader(content)))
            {

                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;


                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));


                reader.ReadToFollowing("layer");


                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                switch (encodingType)
                {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                mapDataString = reader.ReadElementContentAsString().Trim();


                //This is where the object positions are loaded. In order to create the pickups, I copied the information already used for the turrets

                turretPositions.Clear();

                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }


                pickupPositions.Clear();

                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            pickupPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }

            }

            //Based on our decision before, the information from the TMX file is converted into text that unity can understand. In our example, we use Base64
            switch (mapDataFormat)
            {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length)
                    {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines)
                    {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values)
                        {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            //This is to keep all the objects in a grid shape
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // The SpriteSheetTexture stores all the sprites needed to create the scene
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Each sprite is loaded in from the information loaded from the mapdata file
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--)
            {
                for (int x = 0; x < spriteSheetColumns; x++)
                {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // This area creates the tiles for each game object and organizes them into the proper "holders"
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++)
            {
                for (int x = 0; x < mapColumns; x++)
                {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // In order to create my pickup prefabs, I copied the creation of the turret objects. They are instantiated here based on the location given by the Mapdata
        {
            foreach (Vector3 turretPosition in turretPositions)
            {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }

        {
            foreach (Vector3 pickupPosition in pickupPositions)
            {
                GameObject pickup = Instantiate(pickupPrefab, pickupPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                pickup.name = "Pickup";
                pickup.transform.parent = pickupHolder;
            }
        }

        //When the map is loaded, a text will appear on the console showing the date and time
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


