﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{
    // Start is called before the first frame update

    public Vector4 blue = Color.blue;
    public Vector4 red = Color.red;
    public Vector4 green = Color.green;


    public float timer;

    void Start()
    {
        timer = 2;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer = 2;
            ChangeColor();
        }
    }
    void ChangeColor()
    {
        int colorPicker = (int)Random.Range(0, 3.99f);
        switch(colorPicker)
        {
            case 1:
                transform.GetChild(0).GetComponent<SpriteRenderer>().color = blue;
                break;
            case 2:
                transform.GetChild(0).GetComponent<SpriteRenderer>().color = red;
                break;
            case 3:
                transform.GetChild(0).GetComponent<SpriteRenderer>().color = green;
                break;
        }
    }
}
