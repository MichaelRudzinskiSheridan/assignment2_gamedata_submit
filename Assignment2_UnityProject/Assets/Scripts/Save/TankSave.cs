﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    [Serializable]
    //This is the information stored on the tank that will be saved
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;

        public Vector4 color;
        public float highScore;

        //create an array of all the pickups and their colors
        public GameObject[] pickups = new GameObject[99];
        public Color[] pickupsColor = new Color[99];
        

    }

    //Assigns the variables created in the 7th line
    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
        //find all the pickups in the scene
        data.pickups = GameObject.FindGameObjectsWithTag("Pickup");;

      

    }
    private void Update()
    {
        data.highScore += Time.deltaTime;
        print(data.highScore);
    }
    //By serializing the data we are taking the game object and breaking it down into its most basic parts to be loaded into a save file
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        data.color = tank.transform.GetChild(0).GetComponent<SpriteRenderer>().color;
        data.highScore = Time.time;
        jsonString = JsonUtility.ToJson(data);

        //find the color for all the pickups at this current time and set their colour
        for (int i = 0; i < data.pickups.Length; i++)
        {
            data.pickupsColor[i] = data.pickups[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color;
        }

        return (jsonString);
    }
    //When an object is loaded, all of its base data is read from the json file and loaded into the game.
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.transform.GetChild(0).GetComponent<SpriteRenderer>().color = data.color;
        tank.name = "Tank";


        //set all the pickups in the scene to the colour they were at the time of the save file
        for (int i = 0; i < data.pickups.Length; i++)
        {
           
            data.pickups[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }
}