﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }

    //The saveItems are an array of stored saves. Before we save a new one, we must clear the old one
    public void AddObject(string item) {
        saveItems.Add(item);
    }

    //When saving a game, all the objects that have information to be saved are stored as saveableobjects
    //These objects are writted to the save data file
    public void Save() {
        saveItems.Clear();

        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }

        //https://docs.microsoft.com/en-us/dotnet/api/system.io.streamwriter?view=netframework-4.7.2
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    //When loading a scene, the scene is reloaded.
    public void Load() {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }






    //In the case of the game first playing, we exit this function as this would cause an infinite loop if we didnt
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (firstPlay) return;
        //All the functions that must run during a loaded game
        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    //From the game save file, the information stored is loaded into the scene/
    void LoadSaveGameData() {
        //https://docs.microsoft.com/en-us/dotnet/api/system.io.streamreader.peek?view=netframework-4.7.2
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            while (gameDataFileStream.Peek() >= 0) {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

        //This function goes through all items in the savable objects array and deletes them
    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

    //When the information is loaded from the save game file, the gameobjects are created and all the information stored is loaded into them
    void CreateGameObjects() {
        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
